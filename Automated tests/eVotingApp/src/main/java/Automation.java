import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;


public class Automation {
    public static void main(String[] args) throws MalformedURLException {

        DesiredCapabilities dc = new DesiredCapabilities();

        dc.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
        dc.setCapability("platformName", "android");
        dc.setCapability("appPackage", "com.example.zadatak15_atos");
        dc.setCapability("appActivity", ".MainActivity");

        AndroidDriver<AndroidElement> driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), dc);



        MobileElement el1 = (MobileElement) driver.findElementById("com.example.zadatak15_atos:id/button_prijava");
        el1.click();


        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.example.zadatak15_atos:id/button_registracija")));

        MobileElement el2 = (MobileElement) driver.findElementById("com.example.zadatak15_atos:id/button_registracija");
        el2.click();

        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.example.zadatak15_atos:id/text_username")));
        MobileElement el3 = (MobileElement) driver.findElementById("com.example.zadatak15_atos:id/text_username");
        el3.sendKeys("lora3");

        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.example.zadatak15_atos:id/text_email_registracija")));
        MobileElement el4 = (MobileElement) driver.findElementById("com.example.zadatak15_atos:id/text_email_registracija");
        el4.sendKeys("lora3");

        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.example.zadatak15_atos:id/edit_lozinka_registracija")));
        MobileElement el5 = (MobileElement) driver.findElementById("com.example.zadatak15_atos:id/edit_lozinka_registracija");
        el5.sendKeys("lora3");

        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.example.zadatak15_atos:id/button_registracija2")));
        MobileElement el6 = (MobileElement) driver.findElementById("com.example.zadatak15_atos:id/button_registracija2");
        el6.click();

        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.example.zadatak15_atos:id/email_prijava")));
        MobileElement el7 = (MobileElement) driver.findElementById("com.example.zadatak15_atos:id/email_prijava");
        el7.sendKeys("lora3");

        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.example.zadatak15_atos:id/lozinka_prijava")));
        MobileElement el8 = (MobileElement) driver.findElementById("com.example.zadatak15_atos:id/lozinka_prijava");
        el8.sendKeys("lora3");

        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.example.zadatak15_atos:id/button_prijava2")));
        MobileElement el9 = (MobileElement) driver.findElementById("com.example.zadatak15_atos:id/button_prijava2");
        el9.click();




    }

}
