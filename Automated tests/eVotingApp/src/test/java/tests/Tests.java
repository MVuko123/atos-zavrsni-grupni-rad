package tests;

import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class Tests extends BaseClass{

   /* @Test
    public void Login(){


        WebDriverWait wait = new WebDriverWait(driver, 5);


        MobileElement el1 = (MobileElement) driver.findElementById("praksa.atos.evotingsystem:id/emailTextField");
        el1.sendKeys("lora");

        wait.until(ExpectedConditions.elementToBeClickable(By.id("praksa.atos.evotingsystem:id/passwordTextField")));
        MobileElement el2 = (MobileElement) driver.findElementById("praksa.atos.evotingsystem:id/passwordTextField");
        el2.sendKeys("lora");

        wait.until(ExpectedConditions.elementToBeClickable(By.id("praksa.atos.evotingsystem:id/logInButton")));
        MobileElement el3 = (MobileElement) driver.findElementById("praksa.atos.evotingsystem:id/logInButton");
        el3.click();
    }
*/
    @Test
    public void AdminLogin(){

        WebDriverWait wait = new WebDriverWait(driver, 5);

        MobileElement el1 = (MobileElement) driver.findElementById("praksa.atos.evotingsystem:id/adminButton");
        el1.click();

        wait.until(ExpectedConditions.elementToBeClickable(By.id("praksa.atos.evotingsystem:id/emailAdminTextField")));
        MobileElement el2 = (MobileElement) driver.findElementById("praksa.atos.evotingsystem:id/emailAdminTextField");
        el2.sendKeys("lora");

        wait.until(ExpectedConditions.elementToBeClickable(By.id("praksa.atos.evotingsystem:id/passwordAdminTextField")));
        MobileElement el3 = (MobileElement) driver.findElementById("praksa.atos.evotingsystem:id/passwordAdminTextField");
        el3.sendKeys("lora");

        wait.until(ExpectedConditions.elementToBeClickable(By.id("praksa.atos.evotingsystem:id/logInAdminButton")));
        MobileElement el4 = (MobileElement) driver.findElementById("praksa.atos.evotingsystem:id/logInAdminButton");
        el4.click();

        wait.until(ExpectedConditions.elementToBeClickable(By.id("praksa.atos.evotingsystem:id/title_id")));
        MobileElement el5 = (MobileElement) driver.findElementById("praksa.atos.evotingsystem:id/title_id");
        el5.sendKeys("Test");

        wait.until(ExpectedConditions.elementToBeClickable(By.id("praksa.atos.evotingsystem:id/message_id")));
        MobileElement el6 = (MobileElement) driver.findElementById("praksa.atos.evotingsystem:id/message_id");
        el6.sendKeys("Test");

        wait.until(ExpectedConditions.elementToBeClickable(By.id("praksa.atos.evotingsystem:id/sent_button")));
        MobileElement el7 = (MobileElement) driver.findElementById("praksa.atos.evotingsystem:id/sent_button");
        el7.click();

    }


}
