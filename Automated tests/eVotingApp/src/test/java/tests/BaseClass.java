package tests;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class BaseClass {

    public AppiumDriver<MobileElement> driver;

    @BeforeTest
    public void setup(){

        try {
        DesiredCapabilities caps = new DesiredCapabilities();
       // caps.setCapability("platformName", "ANDROID");
        caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "ANDROID");
        caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "9");
        caps.setCapability(MobileCapabilityType.DEVICE_NAME,"AOSP on IA Emulator");
        caps.setCapability(MobileCapabilityType.UDID, "emulator-5554");
        caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60);
        caps.setCapability("appPackage", "praksa.atos.evotingsystem" );
        caps.setCapability("appActivity", ".view.MainActivity");


            URL url = new URL("http://127.0.0.1:4723/wd/hub");

            driver = new AppiumDriver<MobileElement>(url, caps);
           // driver = new AndroidDriver<MobileElement>(url,caps);



        } catch (Exception e) {
            System.out.println("Cause: " +e.getCause());
            System.out.println("Error: "+ e.getMessage());
            e.printStackTrace();
        }

    }

    @Test
    public void SampleTest(){
        System.out.println("Sample Test");

    }

    @AfterTest
    public void teardown(){

    }

}
