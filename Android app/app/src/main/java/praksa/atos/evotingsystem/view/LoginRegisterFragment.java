package praksa.atos.evotingsystem.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import praksa.atos.evotingsystem.R;
import praksa.atos.evotingsystem.databinding.FragmentLoginRegisterBinding;
import praksa.atos.evotingsystem.model.SurveySingleton;
import praksa.atos.evotingsystem.viewmodel.LoginRegisterViewModel;


public class LoginRegisterFragment extends Fragment {
    public LoginRegisterFragment(){}


    private FragmentLoginRegisterBinding binding;
    private LoginRegisterViewModel viewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        viewModel = new ViewModelProvider(this).get(LoginRegisterViewModel.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentLoginRegisterBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        singInListener();
        adminLoginListener();

        return view;
    }

    private void adminLoginListener() {
        binding.adminButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(requireView()).navigate(R.id.action_loginRegisterFragment_to_adminFragment);
            }
        });
    }

    private void singInListener(){
        binding.logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signInUser();
                Navigation.findNavController(requireView()).navigate(R.id.action_loginRegisterFragment_to_queueFragment);
            }
        });
    }

    private void signInUser(){
        viewModel.signInUser(binding.emailTextField.getText().toString(), binding.passwordTextField.getText().toString());
        SurveySingleton.getInstance().setEmail(binding.emailTextField.getText().toString());
        SurveySingleton.getInstance().setRole("user");
    }
}