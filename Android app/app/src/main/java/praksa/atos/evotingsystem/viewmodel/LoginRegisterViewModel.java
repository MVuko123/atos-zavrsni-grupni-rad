package praksa.atos.evotingsystem.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.google.firebase.auth.FirebaseUser;

import praksa.atos.evotingsystem.model.authentication.Authentication;
import praksa.atos.evotingsystem.model.repository.Firestore;

public class LoginRegisterViewModel extends AndroidViewModel {
    private Authentication authentication;
    private Firestore db;
    private FirebaseUser currentUser;
    public LoginRegisterViewModel(@NonNull Application application) {
        super(application);

        authentication = new Authentication(application);
        db = new Firestore();

    }

    public void signInUser(String email, String password){
        authentication.singInUser(email, password);
    }

    public void singInAdmin(String email, String password){
        authentication.singInAdmin("admin@admin.com", "123");
    }

    public String getCurrentUserEmail(){
        return authentication.getCurrentUserInfo().getEmail();
    }


}
