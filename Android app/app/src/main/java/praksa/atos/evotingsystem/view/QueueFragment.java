package praksa.atos.evotingsystem.view;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import praksa.atos.evotingsystem.R;
import praksa.atos.evotingsystem.databinding.FragmentQueueBinding;

public class QueueFragment extends Fragment {

    public QueueFragment() {
        // Required empty public constructor
    }

    private FragmentQueueBinding binding;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentQueueBinding.inflate(inflater, container, false);
        View view = binding.getRoot();


        return view;
    }
}