package praksa.atos.evotingsystem.view;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import praksa.atos.evotingsystem.R;
import praksa.atos.evotingsystem.databinding.FragmentSurveyBinding;
import praksa.atos.evotingsystem.model.SurveySingleton;
import praksa.atos.evotingsystem.model.authentication.Authentication;
import praksa.atos.evotingsystem.model.models.SurveyResponse;
import praksa.atos.evotingsystem.viewmodel.SurveyViewModel;

public class SurveyFragment extends Fragment {

    private FragmentSurveyBinding binding;
    private SurveyFragmentArgs args;
    private SurveyViewModel viewModel;
    private String surveyId;
    private SurveyResponse response;
    private String userEmail;
    private int storyNumber;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = FragmentSurveyBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();


        return view;
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        args = SurveyFragmentArgs.fromBundle(getArguments());
        surveyId = args.getSurveyId();
        userEmail = args.getEmail();
        storyNumber = args.getStoryNumber();
        viewModel = new ViewModelProvider(this).get(SurveyViewModel.class);
        response = new SurveyResponse(surveyId, 0, SurveySingleton.getInstance().getEmail());

        radioGroupListener();

        sendListener(surveyId, response);

        countDown();
    }


    private void sendListener(String surveyId, SurveyResponse response){
        binding.buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addResponsesToSurvey(surveyId, response);
                Toast.makeText(requireContext(), "Response added!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void radioGroupListener(){
        binding.surveyRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i){
                    case R.id.radioButtonValueOne:
                        response.setResponse(1);
                        break;
                    case R.id.radioButtonValueTwo:
                        response.setResponse(2);
                        break;
                    case R.id.radioButtonValueThree:
                        response.setResponse(3);
                        break;
                    case R.id.radioButtonValueFive:
                        response.setResponse(5);
                        break;
                    case R.id.radioButtonValueEight:
                        response.setResponse(8);
                        break;
                    case R.id.radioButtonValueThirteen:
                        response.setResponse(13);
                        break;
                    case R.id.radioButtonValueTwentyOne:
                        response.setResponse(21);
                        break;
                    default:
                        response.setResponse(0);
                        break;
                }

            }
        });
    }

    private void addResponsesToSurvey(String surveyId, SurveyResponse response){
        viewModel.addResponseToSurvey(surveyId, response);
        Log.d("SurveyFragment", "response added successfully");
    }





    private void countDown(){
            new CountDownTimer(10000, 1000) {

                public void onTick(long millisUntilFinished) {
                    binding.textViewRemainingTime.setText("Remaining time: " + millisUntilFinished / 1000);
                }

                public void onFinish() {
                    NavDirections action = SurveyFragmentDirections.actionSurveyFragmentToInformationFragment(surveyId, storyNumber, userEmail);
                    Navigation.findNavController(requireView()).navigate(action);
                }
            }.start();
    }
}

