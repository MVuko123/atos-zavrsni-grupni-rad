package praksa.atos.evotingsystem.model.authentication;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import praksa.atos.evotingsystem.model.models.User;
import praksa.atos.evotingsystem.model.repository.Firestore;


public class Authentication {

    private static final String TAG  = "Authentication";
    private Application application;
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private Firestore db;

    public Authentication(Application application) {
        this.application = application;
        this.mAuth = FirebaseAuth.getInstance();
        this.db = new Firestore();
    }



    public void registerUser(String email, String password){
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(ContextCompat.getMainExecutor(application), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){

                            currentUser = mAuth.getCurrentUser();
                            Log.d(TAG, "onComplete: " + currentUser.toString());
                            User user = new User(currentUser.getUid(), currentUser.getEmail());

                            db.addUserToFirestore(user);
                            Toast.makeText(application, "User register successful!", Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "createUserWithEmail:success");
                        }else{
                            Log.d(TAG, "createUserWithEmail:failure");
                        }
                    }
                });
    }


    public void singInUser(String email, String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(ContextCompat.getMainExecutor(application), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            currentUser = mAuth.getCurrentUser();

                            Toast.makeText(application, "Sign in successful!", Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "signInUserWithEmail:success");
                        }else{
                            registerUser(email, password);
                            Log.d(TAG, "signInUserWithEmail:failure");
                        }
                    }
                });
    }

    public void singInAdmin(String email, String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(ContextCompat.getMainExecutor(application), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            currentUser = mAuth.getCurrentUser();
                            Toast.makeText(application, "Sign in successful!", Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "signInUserWithEmail:success");
                        }else{
                            Log.d(TAG, "signInUserWithEmail:failure");
                        }
                    }
                });
    }

    public FirebaseUser getCurrentUserInfo(){
        return currentUser;
    }
}
