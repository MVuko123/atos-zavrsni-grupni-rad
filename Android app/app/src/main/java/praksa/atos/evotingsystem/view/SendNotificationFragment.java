package praksa.atos.evotingsystem.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.messaging.FirebaseMessaging;

import praksa.atos.evotingsystem.databinding.FragmentSendNotificationBinding;
import praksa.atos.evotingsystem.model.SurveySingleton;
import praksa.atos.evotingsystem.viewmodel.NotificationViewModel;
import praksa.atos.evotingsystem.viewmodel.SurveyViewModel;

public class SendNotificationFragment extends Fragment {


    private FragmentSendNotificationBinding binding;
    private NotificationViewModel viewModel;
    private SurveyViewModel surveyViewModel;
    private String surveyId;
    private int storyNumber;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(NotificationViewModel.class);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseMessaging.getInstance().subscribeToTopic("all");


        surveyViewModel = new ViewModelProvider(this).get(SurveyViewModel.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSendNotificationBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        sendNotificationListener();

        return view;
    }


    private void sendNotificationListener(){
        binding.sentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DocumentReference reference = surveyViewModel.getDocumentReference();
                surveyId = reference.getId();
                SurveySingleton.getInstance().setSurveyId(surveyId);
                SurveySingleton.getInstance().setStoryNumber(Integer.parseInt(binding.userStoryEditText.getText().toString()));
                sendNotifications(surveyId);
                Log.d("SendNotificationFragment", "onClick: notification sent" + surveyId);


                surveyViewModel.createSurvey(reference);
            }
        });
    }

    private void sendNotifications(String surveyId){
        viewModel.sendNotification(binding.userStoryEditText.getText().toString(),surveyId, SendNotificationFragment.this);

    }

}


