package praksa.atos.evotingsystem.model.models;

public class User {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private String email;

    public User(String id, String email) {
        this.id = id;
        this.email = email;
    }
}
