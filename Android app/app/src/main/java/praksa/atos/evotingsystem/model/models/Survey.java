package praksa.atos.evotingsystem.model.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Survey implements Parcelable {
        private String surveyId;

    protected Survey(Parcel in) {
        surveyId = in.readString();
    }

    public static final Creator<Survey> CREATOR = new Creator<Survey>() {
        @Override
        public Survey createFromParcel(Parcel in) {
            return new Survey(in);
        }

        @Override
        public Survey[] newArray(int size) {
            return new Survey[size];
        }
    };

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public Survey(String surveyId) {
        this.surveyId = surveyId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(surveyId);
    }
}
