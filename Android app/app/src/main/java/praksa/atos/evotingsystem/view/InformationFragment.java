package praksa.atos.evotingsystem.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import praksa.atos.evotingsystem.R;
import praksa.atos.evotingsystem.databinding.FragmentInformationBinding;
import praksa.atos.evotingsystem.model.SurveySingleton;
import praksa.atos.evotingsystem.model.models.SurveyResponse;
import praksa.atos.evotingsystem.view.adapter.InformationAdapter;
import praksa.atos.evotingsystem.viewmodel.InformationViewModel;

public class InformationFragment extends Fragment {

    private FragmentInformationBinding binding;
    private InformationViewModel viewModel;
    private InformationFragmentArgs args;
    private String surveyId;
    private InformationAdapter adapter;
    private float average = 0;
    private int storyNumber;
    private String userEmail;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentInformationBinding.inflate(inflater, container, false);
        View view = binding.getRoot();




        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        args = InformationFragmentArgs.fromBundle(getArguments());
        storyNumber = args.getStoryNumber();
        surveyId = args.getSurveyId();
        userEmail = args.getEmail();

        setupRecyclerView();
        loadData(surveyId);
        setStoryNumber();
        checkIfAdmin();
        sendNewListener();



    }

    private void checkIfAdmin() {
        if(SurveySingleton.getInstance().getRole().equals("user")){
            binding.sendAnotherButton.setVisibility(View.GONE);
        }
    }

    private void sendNewListener() {
        binding.sendAnotherButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(requireView()).navigate(R.id.action_informationFragment_to_sendNotificationFragment);
            }
        });
    }

    private void setStoryNumber(){
        String display = "User story #" + storyNumber;
        binding.userStoryTextView.setText(display);
    }

    private void loadData(String surveyId){
        viewModel = new ViewModelProvider(this).get(InformationViewModel.class);

        viewModel.getAllResponses(surveyId).observe(getViewLifecycleOwner(), responses -> {
            float sum = 0;
            if(responses != null){
                for (SurveyResponse response : responses){

                    sum = sum + response.getResponse();
                }

                average = (sum / responses.size());
                binding.averageTextView.setText(String.valueOf(average));

                adapter.setResponses(responses);

            }
        });


    }





    private InformationAdapter setupRecyclerView(){
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        adapter = new InformationAdapter();
        binding.recyclerView.setAdapter(adapter);

        return adapter;
    }
}
