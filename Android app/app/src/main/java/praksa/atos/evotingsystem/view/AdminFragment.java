package praksa.atos.evotingsystem.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import praksa.atos.evotingsystem.R;
import praksa.atos.evotingsystem.databinding.FragmentAdminBinding;
import praksa.atos.evotingsystem.model.SurveySingleton;
import praksa.atos.evotingsystem.viewmodel.LoginRegisterViewModel;

public class AdminFragment extends Fragment {



    public AdminFragment() {
        // Required empty public constructor
    }

    private FragmentAdminBinding binding;
    private LoginRegisterViewModel viewModel;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = new ViewModelProvider(this).get(LoginRegisterViewModel.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentAdminBinding.inflate(inflater, container, false);
        View view = binding.getRoot();


        singInListener();




        return view;
    }

    private void singInListener(){
        binding.logInAdminButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signInAdmin();
                Navigation.findNavController(requireView()).navigate(R.id.action_adminFragment_to_sendNotificationFragment);
            }
        });
    }

    private void signInAdmin(){
        viewModel.singInAdmin(binding.emailAdminTextField.getText().toString(), binding.passwordAdminTextField.getText().toString());
        SurveySingleton.getInstance().setEmail(binding.emailAdminTextField.getText().toString());
        SurveySingleton.getInstance().setRole("admin");
    }
}