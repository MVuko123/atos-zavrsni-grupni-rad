package praksa.atos.evotingsystem.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;

import praksa.atos.evotingsystem.model.authentication.Authentication;
import praksa.atos.evotingsystem.model.models.SurveyResponse;
import praksa.atos.evotingsystem.model.repository.Firestore;

public class SurveyViewModel extends AndroidViewModel {
    Firestore db;
    Authentication auth;

    public SurveyViewModel(@NonNull Application application) {
        super(application);
        auth = new Authentication(application);
        db = new Firestore();
    }

    public DocumentReference getDocumentReference(){
        return db.getDocumentReference();
    }

    public void createSurvey(DocumentReference reference){
        db.createSurvey(reference);
    }

    public void addResponseToSurvey(String surveyId, SurveyResponse response){
        db.addResponsesToSurvey(surveyId, response);
    }

    public FirebaseUser getCurrentUserInfo(){
        return auth.getCurrentUserInfo();
    }


}
