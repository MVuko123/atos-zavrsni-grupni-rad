package praksa.atos.evotingsystem.model.cloudMessaging;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import praksa.atos.evotingsystem.R;
import praksa.atos.evotingsystem.view.SendNotificationFragment;

public class FcmNotificationsSender  {

    String userFcmToken;
    String storyNumber;
    String surveyId;
    Context mContext;
    Activity mActivity;
    Fragment mFragment;


    private RequestQueue requestQueue;
    private final String postUrl = "https://fcm.googleapis.com/fcm/send";
    private final String fcmServerKey ="AAAAoiMGBww:APA91bEXrDRCfSjhm5QsqWTskeB8ItNpZkAb7tJQaMI7f4h3jiPOOCHtUzcBgzYb6taoqe94gkNVA4V96QtLqSB4OIwriUhV5Pfxb1y3zNKzISDouOnapeRM6Z73GNSAxquLcsSitnAc";


    public FcmNotificationsSender(String userFcmToken, String storyNumber,String surveyId, Application application, SendNotificationFragment sendNotificationFragment) {
        this.userFcmToken = userFcmToken;
        this.storyNumber = storyNumber;
        this.surveyId = surveyId;
        this.mContext = application;
        this.mFragment = sendNotificationFragment;
    }

    public void SendNotifications() {

        requestQueue = Volley.newRequestQueue(mFragment.getActivity().getApplicationContext());
        JSONObject mainObj = new JSONObject();
        try {
            mainObj.put("to", userFcmToken);
            JSONObject notiObject = new JSONObject();
            notiObject.put("title", storyNumber);
            notiObject.put("body", surveyId);
            notiObject.put("icon", R.drawable.atos); // enter icon that exists in drawable only



            mainObj.put("notification", notiObject);


            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, postUrl, mainObj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    // code run is got response

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // code run is got error

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {


                    Map<String, String> header = new HashMap<>();
                    header.put("content-type", "application/json");
                    header.put("authorization", "key=" + fcmServerKey);
                    return header;


                }
            };
            requestQueue.add(request);


        } catch (JSONException e) {
            e.printStackTrace();
        }




    }
}
