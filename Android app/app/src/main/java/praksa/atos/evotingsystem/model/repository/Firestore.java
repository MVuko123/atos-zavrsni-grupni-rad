package praksa.atos.evotingsystem.model.repository;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import praksa.atos.evotingsystem.model.SurveySingleton;
import praksa.atos.evotingsystem.model.models.Survey;
import praksa.atos.evotingsystem.model.models.SurveyResponse;
import praksa.atos.evotingsystem.model.models.User;

public class Firestore {

    private static final String TAG = "Firestore";
    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    private MutableLiveData<List<SurveyResponse>> responses = new MutableLiveData<>();

    public void addUserToFirestore(User user){
        db.collection("users")
                .add(user)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "user added");
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "error adding user");
                    }
                });
    }

    public DocumentReference getDocumentReference(){
        return db.collection("surveys").document();
    }

    public void createSurvey(DocumentReference reference){

        Survey survey = new Survey(reference.getId());

        reference.set(survey);
    }

    public void addResponsesToSurvey(String surveyId, SurveyResponse response){
        db.collection("surveys").document(surveyId)
                .collection("responses")
                .add(response)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference reference) {
                        Log.d(TAG, "response added");
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "response adding failure");
                    }
                });
    }

    public MutableLiveData<List<SurveyResponse>> getAllResponses(String surveyId){
        db.collection("surveys")
                .document(surveyId)
                .collection("responses")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        responses.postValue(queryDocumentSnapshots.toObjects(SurveyResponse.class));
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "onFailure: failed getting responses");
                    }
                });
        return responses;
    }




}
