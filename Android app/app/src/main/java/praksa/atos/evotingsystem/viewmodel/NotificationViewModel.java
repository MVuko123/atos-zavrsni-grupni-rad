package praksa.atos.evotingsystem.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import praksa.atos.evotingsystem.model.cloudMessaging.FcmNotificationsSender;
import praksa.atos.evotingsystem.view.SendNotificationFragment;


public class NotificationViewModel extends AndroidViewModel {



    public NotificationViewModel(@NonNull Application application) {
        super(application);
    }

    public void sendNotification(String storyNumber,String surveyId, SendNotificationFragment sendNotificationFragment){
        if(!storyNumber.isEmpty() && !surveyId.isEmpty()){
            FcmNotificationsSender notificationsSender = new FcmNotificationsSender(
                    "/topics/all",
                    storyNumber,
                    surveyId,
                    getApplication(),
                    sendNotificationFragment);
            notificationsSender.SendNotifications();

        }else{

        }
    }




}
