package praksa.atos.evotingsystem.view.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;



import java.util.ArrayList;
import java.util.List;

import praksa.atos.evotingsystem.R;
import praksa.atos.evotingsystem.model.models.SurveyResponse;

public class InformationAdapter extends RecyclerView.Adapter<InformationAdapter.InformationHolder> {

    private List<SurveyResponse> responses = new ArrayList<>();

    public void setResponses(List<SurveyResponse> responses){
        this.responses = responses;
        Log.d("InformationAdapter", "setResponses: success");
        notifyDataSetChanged();
    }



    @NonNull
    @Override
    public InformationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_info, parent, false);

        return new InformationHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull InformationHolder holder, int position) {

        SurveyResponse currentResponse = responses.get(position);

        holder.textViewEmail.setText(currentResponse.getUserEmail());
        holder.textViewValue.setText(String.valueOf(currentResponse.getResponse()));
        holder.relativeLayout.setContentDescription(currentResponse.getUserEmail());

    }

    @Override
    public int getItemCount() {
        return responses.size();
    }

    class InformationHolder extends RecyclerView.ViewHolder {

        private TextView textViewEmail;
        private TextView textViewValue;
        private RelativeLayout relativeLayout;


        public InformationHolder(View itemView) {
            super(itemView);

            relativeLayout = itemView.findViewById(R.id.relativeLayout);
            textViewEmail = itemView.findViewById(R.id.emailTextView);
            textViewValue = itemView.findViewById(R.id.valueTextView);

        }
    }
}
