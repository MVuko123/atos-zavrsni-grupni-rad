package praksa.atos.evotingsystem.model.models;

public class SurveyResponse {

    private String surveyId;
    private int response;
    private String userEmail;

    public SurveyResponse(String surveyId, int response, String userEmail) {
        this.surveyId = surveyId;
        this.response = response;
        this.userEmail = userEmail;
    }

    @Override
    public String toString() {
        return "SurveyResponse{" +
                "surveyId='" + surveyId + '\'' +
                ", response=" + response +
                ", userEmail='" + userEmail + '\'' +
                '}';
    }

    public SurveyResponse(){}

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getSurveyId() {
        return surveyId;
    }

    public int getResponse() {
        return response;
    }

    public void setResponse(int response) {
        this.response = response;
    }
}
