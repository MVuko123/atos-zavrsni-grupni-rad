package praksa.atos.evotingsystem.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import praksa.atos.evotingsystem.model.models.SurveyResponse;
import praksa.atos.evotingsystem.model.repository.Firestore;

public class InformationViewModel extends AndroidViewModel {
    private Firestore db;

    public InformationViewModel(@NonNull Application application) {
        super(application);

        db = new Firestore();

    }

    public MutableLiveData<List<SurveyResponse>> getAllResponses(String surveyId){

        return db.getAllResponses(surveyId);
    }


}
