package praksa.atos.evotingsystem.model;

import android.hardware.lights.LightState;

import java.util.ArrayList;
import java.util.List;

import praksa.atos.evotingsystem.model.models.SurveyResponse;

public class SurveySingleton {

    private String surveyId;
    private String email;
    private int storyNumber;
    private String role;


    private static SurveySingleton INSTANCE;

    private SurveySingleton(){}


    public static SurveySingleton getInstance(){
        if(INSTANCE == null){
            INSTANCE = new SurveySingleton();
        }
        return INSTANCE;
    }


    public String getSurveyId() {
        return this.surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public int getStoryNumber() {
        return storyNumber;
    }

    public void setStoryNumber(int storyNumber) {
        this.storyNumber = storyNumber;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
