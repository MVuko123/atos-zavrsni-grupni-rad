# eVoting-system

Atos praksa eVoting System projekt

##How to use
User can log in as admin and as regular user. In order to log in with admin role, users has to tap Admin login button.

Admin email example is admin@admin.com with password 123. To log in with regular user role, user enters email in form example@example.com with password that user chooses.

If regular user with entered email does not exist, new user will be created and logged in. Next time email is entered, user will be logged in.

After logging in, admin user is navigated to Send notification screen. Regular users are navigated to Queue screen. Admin enters user story number and when Send notification button is tapped, notification is sent to all users.

Opening notification navigates users to Survey screen where they have 10 seconds to pick one of the proposed values in RadioGroup. Pressing the Send button, survey response is sent.

After 10 seconds, all users are navigated to Information screen where results are shown, below individual responses is average of votes. Admin user has New survey button visible.

When New survey button is tapped, admin is navigated to Send notification screen where new survey can be sent.